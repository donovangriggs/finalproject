import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import {routes} from './routes'
import store from './store/store'
import VueResource from 'vue-resource'

Vue.use(VueRouter)
Vue.use(VueResource)

Vue.http.options.root = 'https://vuestocks-f06b7.firebaseio.com'


Vue.filter('currency', val =>{
  return '$' + val.toLocaleString()
})

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})
